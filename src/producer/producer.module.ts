import { Module } from '@nestjs/common';
import { ProducerService } from './producer.service';
import { BullModule } from '@nestjs/bull';

@Module({
  imports: [
    // CacheModule.register({
    //   host: config.CACHE_QUEUE_URL,
    // }),
    // HttpModule.register({ timeout: 5000, maxRedirects: 5 }),
    BullModule.registerQueue({
      name: 'admin',
    }),
  ],
  providers: [ProducerService],
})
export class ProducerModule {}
