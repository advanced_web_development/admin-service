import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class StudentInfo {
  @ApiProperty({
    type: String,
    default: '2010232',
  })
  @IsString()
  @IsNotEmpty()
  studentId: string;

  @ApiProperty({
    type: Number,
    default: 2,
  })
  @IsNumber()
  @IsNotEmpty()
  userId: number;
}
