import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  Res,
  StreamableFile,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { MappingService } from './mapping.service';
import {
  GetMappedAccountReq,
  MapStudent,
  MapStudentReqDto,
  UnmapStudent,
  UploadStudentReqDto,
} from './dtos/mapping.dto';
import { AdminGuard } from 'src/auth/guard/admin-guard.guard';
import { Response } from 'express';
@ApiTags('mapping')
@Controller('mapping')
@ApiBearerAuth()
export class MappingController {
  constructor(private mappingService: MappingService) {}

  @UseGuards(AdminGuard)
  @Post('/map-student-list')
  @ApiResponse({
    type: 'Succesfully map student list!',
  })
  async uploadStudent(
    @Req() request: Request,
    @Body() mapStudentReqDto: UploadStudentReqDto,
  ) {
    const userId: number = parseInt(request['user']);
    const result = this.mappingService.uploadStudent(mapStudentReqDto);
    return result;
  }

  @UseGuards(AdminGuard)
  @Post('/unmap')
  // @ApiResponse({
  //   type: 'Succesfully uploaded student list!',
  // })
  async unMapStudent(
    @Req() request: Request,
    @Body() mapStudentReqDto: UnmapStudent,
  ) {
    const userId: number = parseInt(request['user'].userId);
    const result = this.mappingService.unmapStudent(mapStudentReqDto);
    return result;
  }

  @UseGuards(AdminGuard)
  @Post('/map')
  // @ApiResponse({
  //   type: 'Succesfully uploaded student list!',
  // })
  async mapStudent(
    @Req() request: Request,
    @Body() mapStudentReqDto: MapStudent,
  ) {
    const userId: number = parseInt(request['user'].userId);
    const result = this.mappingService.mapStudent(mapStudentReqDto);
    return result;
  }

  @UseGuards(AdminGuard)
  @Get('/all-mapped-account')
  async getMappedAccount(@Req() request: Request) {
    const userId: number = parseInt(request['user'].userId);
    const result = this.mappingService.getMappedAccount();
    return result;
  }

  @UseGuards(AdminGuard)
  @Get('/all-students')
  async getAllStudent(@Req() request: Request) {
    const userId: number = parseInt(request['user'].userId);
    const result = this.mappingService.getAllStudent();
    return result;
  }

  @UseGuards(AdminGuard)
  @Get('/template')
  async getTemplate(
    @Res({ passthrough: true }) res: Response,
  ): Promise<StreamableFile> {
    const file = await this.mappingService.chooseTemplate();
    res.set({
      'Content-Type': 'application/xlsx',
      'Content-Disposition': 'attachment; filename="StudentListTemplate.xlsx"',
    });
    return new StreamableFile(file);
  }
}
