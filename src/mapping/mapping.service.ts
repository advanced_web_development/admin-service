import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import {
  GetMappedAccountReq,
  MapStudent,
  UnmapStudent,
  UploadStudentReqDto,
} from './dtos/mapping.dto';
const ExcelJS = require('exceljs/dist/es5');
@Injectable()
export class MappingService {
  constructor(private prismaService: PrismaService) {}
  async uploadStudent(mapStudentReqDto: UploadStudentReqDto) {
    const mapList = mapStudentReqDto.mapList;
    const invalidUsers: number[] = [];
    const invalidStudents: string[] = [];
    const mappedList: { userId: number; studentId: string }[] = [];
    for (const map of mapList) {
      // console.log(map);
      const user = await this.prismaService.user.findUnique({
        where: { id: map.userId },
      });
      if (!user) {
        invalidUsers.push(map.userId);

        continue;
      }
      // const student = await this.prismaService.studentClass.findFirst({
      //   where: {
      //     student_id: map.studentId,
      //   },
      // });
      // if (!student) {
      //   invalidStudents.push(map.studentId);

      //   continue;
      // }
      // const mappedUser = await this.prismaService.user.findFirst({
      //   where: {
      //     student_id: map.studentId,
      //   },
      // });
      // if (mappedUser) {
      //   console.log('3');
      //   const update = await this.prismaService.user.updateMany({
      //     where: {
      //       student_id: map.studentId,
      //     },
      //     data: {
      //       student_id: null,
      //     },
      //   });
      // }
      const update = await this.prismaService.user.updateMany({
        where: {
          student_id: map.studentId,
        },
        data: {
          student_id: null,
        },
      });
      try {
        const update = await this.prismaService.user.update({
          where: {
            id: map.userId,
          },
          data: {
            student_id: map.studentId,
          },
        });
        mappedList.push(map);
      } catch (error) {
        console.log(error);
      }
    }
    return { mappedList, invalidUsers, invalidStudents };
  }
  async unmapStudent(unmapStudents: UnmapStudent) {
    const user = await this.prismaService.user.findUnique({
      where: { id: unmapStudents.userId },
    });

    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    if (!user.student_id) {
      throw new HttpException(
        'This user is currently not mapped with any student id',
        HttpStatus.NOT_FOUND,
      );
    }

    const update = await this.prismaService.user.update({
      where: {
        id: unmapStudents.userId,
      },
      data: {
        student_id: null,
      },
    });
    return update;
  }

  async mapStudent(mapStudents: MapStudent) {
    const user = await this.prismaService.user.findUnique({
      where: { id: mapStudents.userId },
    });

    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    const student = await this.prismaService.studentClass.findFirst({
      where: {
        student_id: mapStudents.studentId,
      },
    });
    if (!student) {
      throw new HttpException('Student Id not found', HttpStatus.NOT_FOUND);
    }
    const update = await this.prismaService.user.update({
      where: {
        id: mapStudents.userId,
      },
      data: {
        student_id: student.student_id,
      },
    });
    return update;
  }

  async getMappedAccount() {
    const users = await this.prismaService.user.findMany({
      where: {
        student_id: { not: null },
      },
    });
    // console.log(users);
    return users;
  }
  async getAllStudent() {
    const students = await this.prismaService
      .$queryRaw`select distinct("StudentClass".student_id), "User".id as user_id from "StudentClass"
    left join "User" on "User".student_id ="StudentClass".student_id`;
    // console.log(students);
    return students;
  }

  async chooseTemplate() {
    const workbook = new ExcelJS.Workbook();
    const sheet = workbook.addWorksheet('My Exam Result');
    sheet.columns = [
      { header: 'StudentID', key: 'StudentID', width: 30 },
      { header: 'UserID', key: 'UserID', width: 30 },
    ];

    const buffer = await workbook.xlsx.writeBuffer();
    return buffer;
  }
}
