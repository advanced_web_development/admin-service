import { ApiProperty } from '@nestjs/swagger';

export class FilterUsersDto {
  @ApiProperty({
    default: 'first name',
    nullable: true,
  })
  firstName: string;

  @ApiProperty({
    default: 'last name',
    nullable: true,
  })
  lastName: string;

  @ApiProperty({
    default: 'phone number',
    nullable: true,
  })
  phoneNumber: string;

  @ApiProperty({
    default: 'student id',
    nullable: true,
  })
  studentId: string;

  @ApiProperty({
    default: 'email',
    nullable: true,
  })
  email: string;

  @ApiProperty({
    default: 'desc',
    nullable: true,
  })
  sortByIdType: string;

  @ApiProperty({
    default: '',
    nullable: true,
  })
  lastUserId: number;

  @ApiProperty({
    default: 'user ID',
    nullable: true,
  })
  userId: number;
}
