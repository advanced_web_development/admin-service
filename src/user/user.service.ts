import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { FilterUsersDto } from './dtos/GetUserDto';

@Injectable()
export class UserService {
  constructor(private prismaService: PrismaService) {}

  async getUsersWithPagination(filterDto: FilterUsersDto) {
    const pageSize = 10;
    const sortByIdType = filterDto.sortByIdType === 'desc' ? 'desc' : 'asc';

    let users = [];
    let lastUserId = null;

    if (sortByIdType === 'desc') {
      const largestId = await this.prismaService.user.aggregate({
        _max: {
          id: true,
        },
      });
      users = await this.prismaService.user.findMany({
        where: {
          first_name: {
            contains: filterDto.firstName,
            mode: 'insensitive',
          },
          last_name: {
            contains: filterDto.lastName,
            mode: 'insensitive',
          },
          phone_number: {
            contains: filterDto.phoneNumber,
            mode: 'insensitive',
          },
          student_id: {
            contains: filterDto.studentId,
            mode: 'insensitive',
          },
          email: {
            contains: filterDto.email,
            mode: 'insensitive',
          },
          id: filterDto.userId
            ? filterDto.userId
            : {
                lt: filterDto.lastUserId
                  ? filterDto.lastUserId
                  : largestId._max.id + 1,
              },
        },
        orderBy: {
          id: 'desc',
        },
        take: pageSize,
      });
      if (users.length > 0) {
        lastUserId = users[users.length - 1].id;
      }
    } else {
      const earliestClass = await this.prismaService.class.findFirst();
      const beforeEarliestCreatedDate = new Date(earliestClass.created_at);
      beforeEarliestCreatedDate.setDate(
        beforeEarliestCreatedDate.getDate() - 1,
      );
      users = await this.prismaService.user.findMany({
        where: {
          first_name: {
            contains: filterDto.firstName,
            mode: 'insensitive',
          },
          last_name: {
            contains: filterDto.lastName,
            mode: 'insensitive',
          },
          phone_number: {
            contains: filterDto.phoneNumber,
            mode: 'insensitive',
          },
          student_id: {
            contains: filterDto.studentId,
            mode: 'insensitive',
          },
          email: {
            contains: filterDto.email,
            mode: 'insensitive',
          },
          id: filterDto.userId
            ? filterDto.userId
            : {
                gt: filterDto.lastUserId ? filterDto.lastUserId : -1,
              },
        },
        orderBy: {
          id: 'asc',
        },
        take: pageSize,
      });
      if (users.length > 0) {
        lastUserId = users[users.length - 1].id;
      }
    }

    let totalCount = 0;
    if (filterDto.userId) {
      totalCount = await this.prismaService.user.count({
        where: {
          first_name: {
            contains: filterDto.firstName,
            mode: 'insensitive',
          },
          last_name: {
            contains: filterDto.lastName,
            mode: 'insensitive',
          },
          phone_number: {
            contains: filterDto.phoneNumber,
            mode: 'insensitive',
          },
          student_id: {
            contains: filterDto.studentId,
            mode: 'insensitive',
          },
          email: {
            contains: filterDto.email,
            mode: 'insensitive',
          },
          id: filterDto.userId,
        },
      });
    } else {
      totalCount = await this.prismaService.user.count({
        where: {
          first_name: {
            contains: filterDto.firstName,
            mode: 'insensitive',
          },
          last_name: {
            contains: filterDto.lastName,
            mode: 'insensitive',
          },
          phone_number: {
            contains: filterDto.phoneNumber,
            mode: 'insensitive',
          },
          student_id: {
            contains: filterDto.studentId,
            mode: 'insensitive',
          },
          email: {
            contains: filterDto.email,
            mode: 'insensitive',
          },
        },
      });
    }

    const totalPages = Math.ceil(totalCount / pageSize);

    return {
      users: users,
      lastUserId: lastUserId,
      totalPages: totalPages,
      totalUsers: totalCount,
    };
  }

  async toggleBanUser(userId: number) {
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
    });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    }
    const updatedUser = await this.prismaService.user.update({
      where: {
        id: userId,
      },
      data: {
        is_banned: !user.is_banned,
      },
    });
    return updatedUser;
  }

  async mappingStudentIdForAStudent(userId: number, studentId: string) {
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
    });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    }

    if (!studentId) {
      const updatedUser = await this.prismaService.user.update({
        where: {
          id: userId,
        },
        data: {
          student_id: null,
        },
      });

      return updatedUser;
    }

    const userWithStudentId = await this.prismaService.user.findUnique({
      where: {
        student_id: studentId,
      },
    });

    if (userWithStudentId) {
      // User exists, proceed with the update
      await this.prismaService.user.update({
        where: {
          student_id: studentId,
        },
        data: {
          student_id: null,
        },
      });
    }

    const updatedUser = await this.prismaService.user.update({
      where: {
        id: userId,
      },
      data: {
        student_id: studentId,
      },
    });

    return updatedUser;
  }
}
