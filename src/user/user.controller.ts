import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/auth/guard/admin-guard.guard';
import { FilterUsersDto } from './dtos/GetUserDto';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @Get('/all')
  async getAll(@Query() filterDto: FilterUsersDto) {
    if (filterDto.userId) {
      if (typeof +filterDto.userId !== 'number') {
        throw new HttpException('Wrong user id type', HttpStatus.BAD_REQUEST);
      }
    }
    if (filterDto.lastUserId) {
      if (typeof +filterDto.lastUserId !== 'number') {
        throw new HttpException(
          'Wrong last user id type',
          HttpStatus.BAD_REQUEST,
        );
      }
      filterDto.lastUserId = +filterDto.lastUserId;
    }
    filterDto.userId = +filterDto.userId;
    return await this.userService.getUsersWithPagination(filterDto);
  }

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @Put('/ban/:id')
  async toggleBanUser(@Param('id') userId: number) {
    if (typeof +userId !== 'number') {
      throw new HttpException('Wrong user id type', HttpStatus.BAD_REQUEST);
    }
    return await this.userService.toggleBanUser(+userId);
  }

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @Post('/mapping-student-id/:id')
  async mappingStudentIdForAStudent(
    @Param('id') userId: number,
    @Body() payload: { studentId: string },
  ) {
    if (typeof +userId !== 'number') {
      throw new HttpException('Wrong user id type', HttpStatus.BAD_REQUEST);
    }
    return await this.userService.mappingStudentIdForAStudent(
      +userId,
      payload.studentId,
    );
  }
}
