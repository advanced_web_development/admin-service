import { Controller, Put, Get, Query, UseGuards, Param } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ClassService } from './class.service';
import { FilterClassesDto } from './dtos/GetClassDto';
import { AdminGuard } from 'src/auth/guard/admin-guard.guard';

@ApiTags('Class')
@Controller('class')
export class ClassController {
  constructor(private classService: ClassService) {}

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @Get('/all')
  async getAll(@Query() filterDto: FilterClassesDto) {
    return await this.classService.getClassesWithPagination(filterDto);
  }

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @Put('/activate/:id')
  async toggleActivateClass(@Param('id') classId: string) {
    return await this.classService.toggleActivateClass(classId);
  }
}
