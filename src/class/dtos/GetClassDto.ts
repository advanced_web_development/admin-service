import { ApiProperty } from '@nestjs/swagger';

export class FilterClassesDto {
  @ApiProperty({
    default: 'name',
    nullable: true,
  })
  name: string;

  @ApiProperty({
    default: 'section',
    nullable: true,
  })
  section: string;

  @ApiProperty({
    default: 'room',
    nullable: true,
  })
  room: string;

  @ApiProperty({
    default: 'subject',
    nullable: true,
  })
  subject: string;

  @ApiProperty({
    default: 'desc',
    nullable: true,
  })
  sortByCreatedDateType: string;

  @ApiProperty({
    default: '',
    nullable: true,
  })
  lastCreatedDate: string;

  @ApiProperty({
    default: 'class ID',
    nullable: true,
  })
  id: string;
}
