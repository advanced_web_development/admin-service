import { Module } from '@nestjs/common';
import { ClassController } from './class.controller';
import { ClassService } from './class.service';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  controllers: [ClassController],
  providers: [ClassService],
  imports: [PrismaModule],
})
export class ClassModule {}
