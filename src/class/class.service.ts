import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { FilterClassesDto } from './dtos/GetClassDto';

@Injectable()
export class ClassService {
  constructor(private prismaService: PrismaService) {}

  async getClassesWithPagination(filterDto: FilterClassesDto) {
    const pageSize = 10;
    const sortByCreatedDateType =
      filterDto.sortByCreatedDateType === 'desc' ? 'desc' : 'asc';

    let classes = [];
    let lastCreatedDate = null;

    if (sortByCreatedDateType === 'desc') {
      let nextDate = new Date();
      nextDate.setDate(nextDate.getDate() + 1);
      classes = await this.prismaService.class.findMany({
        where: {
          created_at: {
            lt: filterDto.lastCreatedDate
              ? new Date(filterDto.lastCreatedDate)
              : nextDate,
          },
          name: {
            contains: filterDto.name,
            mode: 'insensitive',
          },
          section: {
            contains: filterDto.section,
            mode: 'insensitive',
          },
          room: {
            contains: filterDto.room,
            mode: 'insensitive',
          },
          subject: {
            contains: filterDto.subject,
            mode: 'insensitive',
          },
          id: filterDto.id,
        },
        orderBy: {
          created_at: 'desc',
        },
        take: pageSize,
      });
      if (classes.length > 0) {
        lastCreatedDate = classes[classes.length - 1].created_at;
      }
    } else {
      const earliestClass = await this.prismaService.class.findFirst();
      const beforeEarliestCreatedDate = new Date(earliestClass.created_at);
      beforeEarliestCreatedDate.setDate(
        beforeEarliestCreatedDate.getDate() - 1,
      );
      classes = await this.prismaService.class.findMany({
        where: {
          created_at: {
            gt: filterDto.lastCreatedDate
              ? new Date(filterDto.lastCreatedDate)
              : beforeEarliestCreatedDate,
          },
          name: {
            contains: filterDto.name,
            mode: 'insensitive',
          },
          section: {
            contains: filterDto.section,
            mode: 'insensitive',
          },
          room: {
            contains: filterDto.room,
            mode: 'insensitive',
          },
          subject: {
            contains: filterDto.subject,
            mode: 'insensitive',
          },
          id: filterDto.id,
        },
        orderBy: {
          created_at: 'asc',
        },
        take: pageSize,
      });
      if (classes.length > 0) {
        lastCreatedDate = classes[classes.length - 1].created_at;
      }
    }

    const totalCount = await this.prismaService.class.count({
      where: {
        name: {
          contains: filterDto.name,
          mode: 'insensitive',
        },
        section: {
          contains: filterDto.section,
          mode: 'insensitive',
        },
        room: {
          contains: filterDto.room,
          mode: 'insensitive',
        },
        subject: {
          contains: filterDto.subject,
          mode: 'insensitive',
        },
        id: filterDto.id,
      },
    });

    const totalPages = Math.ceil(totalCount / pageSize);

    return {
      classes: classes,
      lastCreatedDate,
      totalPages: totalPages,
      totalClasses: totalCount,
    };
  }

  async toggleActivateClass(classId: string) {
    const foundClass = await this.prismaService.class.findUnique({
      where: {
        id: classId,
      },
    });
    if (!foundClass) {
      throw new HttpException('Class not found', HttpStatus.BAD_REQUEST);
    }
    const updatedClass = await this.prismaService.class.update({
      where: {
        id: classId,
      },
      data: {
        is_activate: !foundClass.is_activate,
      },
    });
    return updatedClass;
  }
}
