import { readFileSync } from 'fs';

require('dotenv').config();

class Config {
  MAIL_QUEUE_URL = process.env.MAIL_QUEUE_URL;
  CACHE_QUEUE_URL = process.env.CACHE_QUEUE_URL;
  REFRESH_TOKEN_EXPIRED_TIME = parseInt(process.env.REFRESH_TOKEN_EXPIRED_TIME);
  ACCESS_TOKEN_EXPIRED_TIME = parseInt(process.env.ACCESS_TOKEN_EXPIRED_TIME);
}
export const accTokenPrivateKey = readFileSync(
  './keys/access/private.pem',
  'utf8',
);
export const _config = new Config();
export const accTokenPublicKey = readFileSync(
  './keys/access/public.pem',
  'utf8',
);

export const refTokenPrivateKey = readFileSync(
  './keys/refresh/private.pem',
  'utf8',
);
export const refTokenPublicKey = readFileSync(
  './keys/refresh/public.pem',
  'utf8',
);
