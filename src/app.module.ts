import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { PrismaModule } from './prisma/prisma.module';
import { SecurityModule } from './auth/guard/security.module';
import { BullModule } from '@nestjs/bull';
import { _config } from './config';
import { ProducerModule } from './producer/producer.module';
import { ClassModule } from './class/class.module';
import { UserModule } from './user/user.module';
import { MappingModule } from './mapping/mapping.module';

@Module({
  imports: [
    AuthModule,
    PrismaModule,
    SecurityModule,
    BullModule.forRoot({
      redis: _config.MAIL_QUEUE_URL,
    }),
    ProducerModule,
    ClassModule,
    UserModule,
    MappingModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
