import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle('Authentication')
    .setDescription('The Authentication API description')
    .setVersion('1.0')
    .addBearerAuth()
    // .setBasePath('khoa')
    .addServer('/')
    .addServer('https://fitclassroom.buudadawg.online/v1/api/admin')
    // .addServer('/v1/api/gateway')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.enableCors({
    origin: '*',
  });
  await app.listen(5005);
}
bootstrap();
