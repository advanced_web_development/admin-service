import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { JwtModule } from '@nestjs/jwt';
import { _config, accTokenPrivateKey } from 'src/config';
import { HashPasswordModule } from 'src/hash-password/hash-password.module';
import { CacheModule } from 'src/cache/cache.module';

@Module({
  imports: [
    JwtModule.register({
      // secret: accTokenPrivateKey,
      signOptions: { expiresIn: '24h' },
    }),
    CacheModule.register({
      host: _config.CACHE_QUEUE_URL,
    }),
    HashPasswordModule,
    PrismaModule,
  ],
  providers: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
