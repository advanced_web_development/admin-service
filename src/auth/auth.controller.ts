import {
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { SignInDto, SignUpDto } from './dtos/auth.dto';
import { AuthService } from './auth.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard, AdminRefreshGuard } from './guard/admin-guard.guard';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('/sign_up')
  async signUp(@Body() signUpDto: SignUpDto) {
    const result = await this.authService.signUp(signUpDto);
    // await this.authService.confirmMail(signUpDto.email);
    return result;
  }

  @Post('/sign_in')
  @HttpCode(203)
  signIn(@Body() signInDto: SignInDto) {
    return this.authService.signIn(signInDto);
  }
  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @Post('sign_out')
  signOut(@Req() request: Request) {
    const userId: number = request['user'].userId;

    return this.authService.signOut(userId.toString());
    // return user;
  }

  @ApiBearerAuth()
  @Post('/refresh')
  @UseGuards(AdminRefreshGuard)
  refresh(@Req() req: Request) {
    const userId: number = req['user'].id;
    const refresh_token = req['user'].refresh_token;
    // console.log(userId, refresh_token);
    return this.authService.refreshTokens(userId, refresh_token);
  }

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @Get('/test_auth')
  testAuth() {
    return { message: 'Authenticated' };
  }
}
