import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './strategy.jwt';
import { RefreshTokenStrategy } from './admin-refresh.guard';

@Module({
  imports: [PassportModule],
  providers: [JwtStrategy, RefreshTokenStrategy],
})
export class SecurityModule {}
