import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { _config, accTokenPrivateKey, accTokenPublicKey } from 'src/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: accTokenPublicKey,
    });
  }

  async validate(payload: any) {
    return {
      userId: payload.id,
      username: payload.username,
      // email: payload.email,
    };
  }
}
