import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import {
  IsString,
  IsEmail,
  IsNotEmpty,
  IsDateString,
  IsInt,
  IsBoolean,
} from 'class-validator';

export class SignUpDto {
  // @ApiProperty({
  //   default: 'user@gmail.com',
  // })
  // @IsNotEmpty()
  // @IsEmail()
  // email: string;

  @ApiProperty({
    default: 'MyUsername',
  })
  @IsNotEmpty()
  @IsString()
  username: string;

  // @ApiProperty({
  //   default: 'Andrew',
  // })
  // @IsString()
  // first_name: string;

  // @ApiProperty({
  //   default: 'Nguyen',
  // })
  // @IsString()
  // last_name: string;

  // @ApiProperty({
  //   description: 'Please enter a valid TimeStamp, Ex: 1700280869',
  //   default: 1700280869,
  // })
  // @IsInt()
  // birthdate: number;

  @ApiProperty({
    default: 'MyPassword',
  })
  @IsNotEmpty()
  @IsString()
  password: string;

  // @ApiProperty({
  //   default: '012345678',
  // })
  // @IsString()
  // phone_number: string;
}
export class SignInDto {
  @ApiProperty({
    default: 'MyUsername',
  })
  @IsString()
  username: string;

  @ApiProperty({
    default: 'MyPassword',
  })
  @IsString()
  password: string;
}
export class SignInDtoRes {
  @ApiProperty()
  @IsString()
  @Expose()
  refresh_token: string;

  @ApiProperty()
  @IsString()
  @Expose()
  access_token: string;

  @ApiProperty()
  @IsString()
  @Expose()
  username: string;

  @ApiProperty()
  @IsString()
  @Expose()
  email: string;

  @ApiProperty()
  @IsString()
  @Expose()
  birthdate: string;

  @ApiProperty()
  @IsString()
  @Expose()
  first_name: string;

  @ApiProperty()
  @IsString()
  @Expose()
  last_name: string;

  @ApiProperty()
  @IsString()
  @Expose()
  user_id: string;

  @ApiProperty()
  @IsBoolean()
  @Expose()
  is_activated: boolean;

  @ApiProperty()
  @IsString()
  @Expose()
  phone_number: string;
}

export interface UserData {
  username: string;
  userId: number;
  birthdate: Date;
  email: string;
  first_name: string;
  last_name: string;
}
