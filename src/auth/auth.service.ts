import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { SignInDto, SignUpDto, UserData } from './dtos/auth.dto';
import { _config, accTokenPrivateKey, refTokenPrivateKey } from 'src/config';
import { PrismaService } from 'src/prisma/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { HashPasswordService } from 'src/hash-password/hash-password.service';
import { promisify } from 'util';
import { scrypt as _scrypt } from 'crypto';
import { CacheService } from 'src/cache/cache.service';

const scrypt = promisify(_scrypt);

@Injectable()
export class AuthService {
  constructor(
    private prismaService: PrismaService,
    private jwtService: JwtService,
    private cacheService: CacheService,
    private hashPasswordService: HashPasswordService,
    // private mailProducerService: MailProducerService,
  ) {}
  async signIn(signInDto: SignInDto) {
    const _username = signInDto.username;
    const _password = signInDto.password;
    // console.log(_username, _password);
    // if (_username == 'admin' && _password == 'admin') {

    // }
    const user = await this.prismaService.admin_user.findFirst({
      where: {
        username: _username,
      },
    });
    if (!user) {
      throw new HttpException('Invalid username', HttpStatus.UNAUTHORIZED);
    }
    const [salt, storedHash] = user.password.split('.');

    const hash = (await scrypt(_password, salt, 32)) as Buffer;

    if (storedHash === hash.toString('hex')) {
    } else {
      throw new HttpException('Auth failure error', HttpStatus.UNAUTHORIZED);
    }
    const payload = {
      usernane: _username,
      password: _password,
      id: user.id,
    };
    const [access_token, refresh_token] = this.createToken(payload);

    await this.cacheRefreshToken(user.id, refresh_token);

    return {
      access_token,
      refresh_token,
      id: user.id,
    };
  }
  confirmMail(email: string) {
    throw new Error('Method not implemented.');
  }
  async signUp(signUpDto: SignUpDto) {
    const _username = signUpDto.username;
    // const _email = signUpDto.email;
    //Check the username if it existed in the database
    const user = await this.prismaService.admin_user.findFirst({
      where: {
        username: _username,
      },
    });
    //If it does then throw error

    if (user) {
      throw new HttpException(
        'Username already existed',
        HttpStatus.BAD_REQUEST,
      );
    }

    //Else create new user
    //First hash the user password
    const _password = signUpDto.password;
    //hash password
    const hashedPassword =
      await this.hashPasswordService.createHashPassword(_password);

    const _data = {
      // email: signUpDto.email,
      // birthdate: signUpDto.birthdate ? new Date(signUpDto.birthdate) : null,
      password: hashedPassword,
      // first_name: signUpDto.first_name || null,
      // last_name: signUpDto.last_name || null,
      username: signUpDto.username,
      // phone_number: signUpDto.phone_number || null,
    };
    const newUser = await this.prismaService.admin_user.create({ data: _data });
    // console.log(newUser);
    return {
      statusCode: 201,
      message: 'Register account successfully!',
    };
  }

  createToken(user: {
    usernane: string;
    password: string;
    id: number;
  }): [string, string] {
    const payload = user;
    const access_token = this.createAccessToken(payload);
    const refresh_token = this.createRefreshToken(payload);
    return [access_token, refresh_token];
  }

  createAccessToken(user: { usernane: string; password: string; id: number }) {
    const payload = user;
    const refresh_token = this.jwtService.sign(payload, {
      issuer: 'RoadToGraduation',
      subject: 'Access',
      expiresIn: _config.ACCESS_TOKEN_EXPIRED_TIME,
      algorithm: 'RS256',
      privateKey: accTokenPrivateKey,
    });

    return refresh_token;
  }
  createRefreshToken(user: { usernane: string; password: string; id: number }) {
    const payload = user;
    const refresh_token = this.jwtService.sign(payload, {
      issuer: 'RoadToGraduation',
      subject: 'Refresh',
      expiresIn: _config.REFRESH_TOKEN_EXPIRED_TIME,
      algorithm: 'RS256',
      privateKey: refTokenPrivateKey,
    });
    // console.log(refTokenPrivateKey, accTokenPrivateKey);
    return refresh_token;
  }

  async refreshTokens(
    userId: number,
    refreshToken: string,
  ): Promise<{ access_token: string; refresh_token: string; id: number }> {
    const cachedRefToken = await this.cacheService
      .getRedisClient()
      .get('admin:' + userId.toString());
    const user = await this.prismaService.admin_user.findUnique({
      where: {
        id: userId,
      },
    });
    // console.log('-------------------');
    // console.log(cachedRefToken, refreshToken, user);
    if (!user || cachedRefToken !== refreshToken) {
      // console.log('-------------------tiki taka');
      // console.log(cachedRefToken, refreshToken);
      throw new HttpException('Access Denined', HttpStatus.UNAUTHORIZED);
    }
    const payload = {
      usernane: user.username,
      password: user.password,
      id: user.id,
    };
    const [access_token, refresh_token] = this.createToken(payload);

    await this.cacheRefreshToken(user.id, refresh_token);

    return {
      access_token: access_token,
      refresh_token: refresh_token,
      id: user.id,
    };
  }
  async cacheRefreshToken(userId: number, refresh_token: string) {
    const res = await this.cacheService
      .getRedisClient()
      .set('admin:' + userId.toString(), refresh_token, {
        EX: _config.REFRESH_TOKEN_EXPIRED_TIME,
      });
    // console.log('Caching refresh token status: ', res);
  }
  async signOut(user_id: string) {
    const cache = this.cacheService.getRedisClient();
    const isLoggedIn = await cache.get(user_id);
    if (isLoggedIn == null) {
      throw new HttpException(
        'You are already logged out',
        HttpStatus.BAD_REQUEST,
      );
    }
    await cache.del('admin:' + user_id);
    return {
      statusCode: 200,
      message: 'Sign out succesfully',
    };
  }
}
